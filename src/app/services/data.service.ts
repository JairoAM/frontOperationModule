import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  baseUrl: string = "http://localhost:8080/api/"
 
  constructor(private http: HttpClient) { }

  //To make calls using GET
  getData(url: string): Observable<any> {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    return this.http.get(`/api/${url}`, { headers });
  }

  //To make calls using POST
  postData(url: string, data: any): Observable<any> {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post(`/api/${url}`, data, { headers });
  }
}
