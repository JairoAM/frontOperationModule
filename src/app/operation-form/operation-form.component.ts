import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { catchError, finalize, tap } from 'rxjs/operators';
import { DataService } from '../services/data.service';
import { throwError } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-operation-form',
  templateUrl: './operation-form.component.html',
  styleUrl: './operation-form.component.scss',
  providers: [DataService]
})
export class OperationFormComponent implements OnInit {
  operationForm!: FormGroup;
  serverResponse: string = '';
  serverResponseError: boolean = false;
  serverResponseCodeError: string = "";
  loading = false;

  constructor(private formBuilder: FormBuilder, private dataService: DataService, private translate: TranslateService) {
  }

  ngOnInit(): void {
    this.operationForm = this.formBuilder.group({
      x: ['', [Validators.required, Validators.pattern('[0-9]*')]],
      y: ['', [Validators.required, Validators.pattern('[0-9]*')]],
      n: ['', [Validators.required, Validators.pattern('[0-9]*')]]
    });
  }

  calculate() {
    this.serverResponse = '';
    this.serverResponseCodeError = '';
    this.serverResponseError = false;

    if (this.operationForm.invalid || this.loading) {
      return;
    }

    const formData = this.operationForm.value;

    this.loading = true;

    this.dataService.postData("calculateModule", formData).pipe(
      tap(response => {
        console.log('Respuesta:', response);
        this.serverResponse = response.k;
      }),
      catchError(error => {
        console.error('Error en la llamada:', error);
        this.serverResponseError = true;
        if (error?.error?.code && error?.error?.code === "1") {
          this.serverResponseCodeError = '1';
        } else {
          this.serverResponseCodeError = '0';
        }
        this.serverResponseError = true;
        return throwError(() => error);
      }),
      finalize(() => {
        this.loading = false;
      })
    ).subscribe();
  }

  changeLanguage(lang: string) {
    this.translate.use(lang);
  }

  onKeyPress(event: KeyboardEvent): void {
    const charCode = event.which || event.keyCode;
    const charStr = String.fromCharCode(charCode);
    const isNumber = /^\d+$/.test(charStr);
    const isDotOrComma = /[.,]/.test(charStr);
    if (!isNumber || isDotOrComma) {
      event.preventDefault();
    }
  }
}