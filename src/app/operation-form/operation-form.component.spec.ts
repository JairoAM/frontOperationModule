import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OperationFormComponent } from './operation-form.component';
import { DataService } from '../services/data.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('OperationFormComponent', () => {
  let component: OperationFormComponent;
  let fixture: ComponentFixture<OperationFormComponent>;
  let dataService: DataService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OperationFormComponent],
      imports: [
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        MatToolbarModule,
        MatIconModule,
        MatMenuModule,
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule
      ],
      providers: [
        FormBuilder,
        DataService
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(OperationFormComponent);
    component = fixture.componentInstance;
    dataService = TestBed.inject(DataService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should require all fields', () => {
    const x = component.operationForm.get('x');
    const y = component.operationForm.get('y');
    const n = component.operationForm.get('n');
    expect(x?.errors?.['required']).toBeTruthy();
    expect(y?.errors?.['required']).toBeTruthy();
    expect(n?.errors?.['required']).toBeTruthy();
  });

  it('should prevent non-numeric characters', () => {
    const preventDefaultSpy = jasmine.createSpy('preventDefault');
    const event = { keyCode: 65, preventDefault: preventDefaultSpy } as any;
    component.onKeyPress(event);
    expect(preventDefaultSpy).toHaveBeenCalled();
  });

  it('should allow numeric characters', () => {
    const preventDefaultSpy = jasmine.createSpy('preventDefault');
    const event = { keyCode: 49, preventDefault: preventDefaultSpy } as any;
    component.onKeyPress(event);
    expect(preventDefaultSpy).not.toHaveBeenCalled();
  });
});
